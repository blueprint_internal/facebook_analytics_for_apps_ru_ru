﻿1
00:00:05,000 --> 00:00:08,000
アプリ用Facebook Analyticsで

2
00:00:08,000 --> 00:00:12,000
各種レポートにセグメントを簡単に設定できます

3
00:00:12,000 --> 00:00:17,000
これはセグメントを「収入」レポートに
設定する方法です

4
00:00:18,000 --> 00:00:21,000
[収入]をクリックします

5
00:00:21,000 --> 00:00:25,000
レポートにフィルタは適用されておらず 

6
00:00:25,000 --> 00:00:29,000
すべての利用者が表示されています 

7
00:00:29,000 --> 00:00:32,000
セグメントを適用するには
[編集]をクリックします

8
00:00:32,000 --> 00:00:36,000
すると[条件を選択]できるようになり

9
00:00:36,000 --> 00:00:43,000
[イベント] [利用者層] [機器情報] 
[インストールソース] などの条件を選べます

10
00:00:43,000 --> 00:00:51,000
％条件を追加すると 
特定のアクションを取る可能性で絞り込めます

11
00:00:51,000 --> 00:00:57,000
ここでは利用者層を基に 
条件を作成していきます

12
00:00:57,000 --> 00:01:02,000
[国]をパラメータに設定します

13
00:01:02,000 --> 00:01:09,000
フィールドをクリックして
「アメリカ合衆国」を選び

14
00:01:09,000 --> 00:01:13,000
[パーセンタイルを追加]をクリックします

15
00:01:13,000 --> 00:01:17,000
次に%を基準とするイベントを選びます

16
00:01:17,000 --> 00:01:21,000
ここではアプリを起動した人の 

17
00:01:21,000 --> 00:01:25,000
上位10%に設定してみます

18
00:01:27,000 --> 00:01:29,000
次に[適用]をクリックします

19
00:01:29,000 --> 00:01:32,000
チャートが更新され

20
00:01:32,000 --> 00:01:37,000
セグメントのデータが表示されます 

21
00:01:37,000 --> 00:01:41,000
セグメントはまだ保存されていないので 

22
00:01:41,000 --> 00:01:45,000
[その他]をクリックし
[名前を付けて保存]を選びます 

23
00:01:45,000 --> 00:01:49,000
ここでセグメントに名前を付けます

24
00:01:50,000 --> 00:01:53,000
[名前を付けて保存]をクリックします

25
00:01:54,000 --> 00:01:57,000
セグメントに戻ったり 

26
00:01:57,000 --> 00:02:01,000
非表示にしたり編集したりできます

27
00:02:01,000 --> 00:02:05,000
セグメントドロップダウンで
別セグメントを選べます

28
00:02:05,000 --> 00:02:09,000
アプリ用Facebook Analyticsでの
セグメント作成方法でした