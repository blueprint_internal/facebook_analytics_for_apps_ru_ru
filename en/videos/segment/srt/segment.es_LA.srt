﻿1
00:00:05,000 --> 00:00:09,000
Es muy fácil configurar un segmento

2
00:00:09,000 --> 00:00:13,000
en distintos informes en Facebook Analytics for Apps. Voy a mostrarte cómo

3
00:00:13,000 --> 00:00:17,000
configurar un segmento en el informe "Ingresos".

4
00:00:17,000 --> 00:00:21,000
Hago clic en "Ingresos" y

5
00:00:21,000 --> 00:00:25,000
veo que no se usaron filtros en el informe 

6
00:00:25,000 --> 00:00:29,000
y que muestra el 100% de los usuarios. Pero puedo 

7
00:00:29,000 --> 00:00:33,000
aplicar un segmento haciendo clic en "Editar".

8
00:00:33,000 --> 00:00:37,000
A continuación, hago clic en "Seleccionar condición"

9
00:00:37,000 --> 00:00:41,000
y elijo un tipo de condición, como "Evento", "Datos demográficos", 

10
00:00:41,000 --> 00:00:45,000
"Información del dispositivo" u "Origen de instalación". También puedo 

11
00:00:45,000 --> 00:00:49,000
agregar una condición de percentiles para filtrar

12
00:00:49,000 --> 00:00:53,000
a las personas que realizan más a menudo una acción específica. En este ejemplo, 

13
00:00:53,000 --> 00:00:57,000
crearé una condición basada en datos demográficos.

14
00:00:57,000 --> 00:01:01,000
Selecciono el parámetro 

15
00:01:01,000 --> 00:01:05,000
"País". Ahora, 

16
00:01:05,000 --> 00:01:09,000
hago clic en el campo y selecciono "Estados Unidos de América".

17
00:01:09,000 --> 00:01:13,000
Hago clic en "Agregar percentil".

18
00:01:13,000 --> 00:01:17,000
Selecciono el evento en el que quiero basar el percentil.

19
00:01:17,000 --> 00:01:21,000
Por ejemplo, el 10% de las personas 

20
00:01:21,000 --> 00:01:25,000
que más abren la aplicación.

21
00:01:25,000 --> 00:01:29,000
Luego, hago clic en "Aplicar".

22
00:01:29,000 --> 00:01:33,000
El gráfico se actualiza

23
00:01:33,000 --> 00:01:37,000
para mostrar los datos para este segmento. 

24
00:01:37,000 --> 00:01:41,000
Para guardar el segmento, 

25
00:01:41,000 --> 00:01:45,000
hago clic en "Más" y selecciono "Guardar como". Ahora, le asigno 

26
00:01:45,000 --> 00:01:49,000
un nombre al segmento.

27
00:01:49,000 --> 00:01:53,000
Hago clic en "Guardar como".

28
00:01:53,000 --> 00:01:57,000
Si en algún momento 

29
00:01:57,000 --> 00:02:01,000
quiero volver al segmento u ocultarlo, puedo hacer clic en "Ocultar"

30
00:02:01,000 --> 00:02:05,000
o "Editar". Para seleccionar otros segmentos, puedo hacer clic en la lista desplegable de segmentos.

31
00:02:05,000 --> 00:02:09,000
Así se crean segmentos en Analytics for Apps.