﻿1
00:00:05,000 --> 00:00:09,000
Das Einrichten eines Segments in

2
00:00:09,000 --> 00:00:13,000
Facebook Analytics für Apps in verschieden Berichten ist einfach. Ich zeige dir die einzelnen Schritte,

3
00:00:13,000 --> 00:00:17,000
um ein Segment im Bericht „Einnahmen“ einzurichten.

4
00:00:17,000 --> 00:00:21,000
Klicke auf „Einnahmen“,

5
00:00:21,000 --> 00:00:25,000
ich sehe, dass der Bericht derzeit überhaupt nicht gefiltert ist 

6
00:00:25,000 --> 00:00:29,000
und darin 100 % der Nutzer aufgelistet sind. Ich kann jedoch 

7
00:00:29,000 --> 00:00:33,000
ein Segment anwenden, indem ich auf „Bearbeiten“ klicke.

8
00:00:33,000 --> 00:00:37,000
Nun kann ich auf „Bedingung auswählen“ klicken

9
00:00:37,000 --> 00:00:41,000
und verschiedene Arten von Bedingungen auswählen, z. B. „Ereignis“, „Demografische Angaben“, 

10
00:00:41,000 --> 00:00:45,000
„Geräteinformationen“ oder „Installationsquelle“. 

11
00:00:45,000 --> 00:00:49,000
Ich kann auch eine Perzentil-Bedingung hinzufügen, um auf Basis der

12
00:00:49,000 --> 00:00:53,000
Personen zu filtern, die eine bestimmte Handlung am häufigsten durchführen. In diesem Beispiel erstelle ich 

13
00:00:53,000 --> 00:00:57,000
eine Bedingung auf Basis der demografischen Angaben.

14
00:00:57,000 --> 00:01:01,000
Ich wähle den Parameter „Land“ aus.

15
00:01:01,000 --> 00:01:05,000
Ich klicke in das Feld 

16
00:01:05,000 --> 00:01:09,000
und wähle „Vereinigte Staaten von Amerika“ aus.

17
00:01:09,000 --> 00:01:13,000
Dann klicke ich auf „Perzentil hinzufügen“.

18
00:01:13,000 --> 00:01:17,000
Wenn ich darauf geklickt habe, wähle ich das Ereignis aus, auf dem das Perzentil basieren soll.

19
00:01:17,000 --> 00:01:21,000
Angenommen, Personen, die die App starten 

20
00:01:21,000 --> 00:01:25,000
und zu den oberen 10 % der Personen gehören.

21
00:01:25,000 --> 00:01:29,000
Als nächstes klicke ich auf „Anwenden“.

22
00:01:29,000 --> 00:01:33,000
Nun siehst du, dass das Diagramm

23
00:01:33,000 --> 00:01:37,000
aktualisiert wird und die Daten für das Segment angezeigt werden. 

24
00:01:37,000 --> 00:01:41,000
Dieses Segment ist noch nicht gespeichert. Ich klicke also 

25
00:01:41,000 --> 00:01:45,000
auf „Mehr“ und wähle „Speichern unter“. Nun kann ich 

26
00:01:45,000 --> 00:01:49,000
mein Segment benennen.

27
00:01:49,000 --> 00:01:53,000
Klicke auf „Speichern unter“.

28
00:01:53,000 --> 00:01:57,000
Wenn ich später 

29
00:01:57,000 --> 00:02:01,000
zum Segment zurückkehren oder es verbergen möchte, kann ich auf „Verbergen“ oder „Bearbeiten“ klicken.

30
00:02:01,000 --> 00:02:05,000
Außerdem kann ich auf das Segmentmenü klicken, um andere Segmente auszuwählen.

31
00:02:05,000 --> 00:02:09,000
So erstellst du also Segmente in Analytics für Apps.