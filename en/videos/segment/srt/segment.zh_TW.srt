﻿1
00:00:05,000 --> 00:00:09,000
您可以輕鬆地在Facebook應用程式分析工具

2
00:00:09,000 --> 00:00:13,000
為不同的分析報告建立目標族群我現在會說明

3
00:00:13,000 --> 00:00:17,000
如何在「收益」分析報告中建立目標族群

4
00:00:17,000 --> 00:00:21,000
點擊「收益」

5
00:00:21,000 --> 00:00:25,000
就會發現分析報告目前尚未經過篩選 

6
00:00:25,000 --> 00:00:29,000
顯示所有的用戶資料但您可以點擊「編輯」 

7
00:00:29,000 --> 00:00:33,000
套用其中一種目標族群

8
00:00:33,000 --> 00:00:37,000
依序點擊「編輯」及「選擇條件」

9
00:00:37,000 --> 00:00:41,000
即可選擇所需的條件類型，包括「事件」、「人口統計資料」 

10
00:00:41,000 --> 00:00:45,000
「裝置資訊」或「安裝來源」您也可以 

11
00:00:45,000 --> 00:00:49,000
新增百分位數條件

12
00:00:49,000 --> 00:00:53,000
藉此篩選出採取特定動作次數最多的用戶在這個範例中 

13
00:00:53,000 --> 00:00:57,000
我會依照人口統計資料來建立條件

14
00:00:57,000 --> 00:01:01,000
首先 

15
00:01:01,000 --> 00:01:05,000
選擇「國家／地區」參數點擊右側的欄位 

16
00:01:05,000 --> 00:01:09,000
從中選擇「美國」

17
00:01:09,000 --> 00:01:13,000
然後點擊「新增百分位數」

18
00:01:13,000 --> 00:01:17,000
點擊後，選擇百分位數所依據的事件

19
00:01:17,000 --> 00:01:21,000
舉例來說，您可以選擇有啟動應用程式 

20
00:01:21,000 --> 00:01:25,000
而且還是前10%採取此動作的用戶

21
00:01:25,000 --> 00:01:29,000
接著點擊「套用」

22
00:01:29,000 --> 00:01:33,000
現在，您可以看到更新過的圖表

23
00:01:33,000 --> 00:01:37,000
顯示該目標族群的資料 

24
00:01:37,000 --> 00:01:41,000
如果您尚未儲存這個目標族群 

25
00:01:41,000 --> 00:01:45,000
可以點擊「更多」並選擇「另存為」 

26
00:01:45,000 --> 00:01:49,000
為目標族群命名

27
00:01:49,000 --> 00:01:53,000
最後點擊「另存為」即可

28
00:01:53,000 --> 00:01:57,000
而且，只要點擊「編輯」或「隱藏」 

29
00:01:57,000 --> 00:02:01,000
便可隨時返回目標族群

30
00:02:01,000 --> 00:02:05,000
或是隱藏目標族群此外，您也可以點擊目標族群下拉式功能表以選擇其他目標族群

31
00:02:05,000 --> 00:02:09,000
這就是在應用程式分析工具中建立目標族群的方法