﻿1
00:00:05,000 --> 00:00:09,000
Menyiapkan segmen itu mudah di

2
00:00:09,000 --> 00:00:13,000
Facebook Analytics untuk Aplikasi, dalam sejumlah laporan berbeda. Saya akan membahas

3
00:00:13,000 --> 00:00:17,000
cara menyiapkan segmen di laporan "Pendapatan".

4
00:00:17,000 --> 00:00:21,000
Klik "Pendapatan," dan

5
00:00:21,000 --> 00:00:25,000
Saya dapat melihat bahwa laporan untuk saat ini tidak difilter 

6
00:00:25,000 --> 00:00:29,000
sama sekali, dan menampilkan 100% pengguna. Namun, saya dapat 

7
00:00:29,000 --> 00:00:33,000
menerapkan segmen dengan mengeklik "Sunting."

8
00:00:33,000 --> 00:00:37,000
Setelah mengeklik "Sunting," saya dapat mengeklik "Pilih Ketentuan," dan pilih dari

9
00:00:37,000 --> 00:00:41,000
beberapa jenis ketentuan, termasuk "Peristiwa," Demografi," 

10
00:00:41,000 --> 00:00:45,000
"Info Perangkat", atau "Sumber Pemasangan." Saya juga dapat 

11
00:00:45,000 --> 00:00:49,000
menambahkan ketentuan persentil ke filter berdasarkan

12
00:00:49,000 --> 00:00:53,000
orang yang paling banyak mengambil tindakan tertentu. Dalam contoh ini, saya akan 

13
00:00:53,000 --> 00:00:57,000
membuat ketentuan berdasarkan demografi.

14
00:00:57,000 --> 00:01:01,000
Saya akan memilih parameter 

15
00:01:01,000 --> 00:01:05,000
"Negara." Saya akan 

16
00:01:05,000 --> 00:01:09,000
mengeklik bidang dan memilih "Amerika Serikat."

17
00:01:09,000 --> 00:01:13,000
Selanjutnya, saya akan mengeklik "Tambahkan Persentil."

18
00:01:13,000 --> 00:01:17,000
Setelah saya mengekliknya, saya akan memilih peristiwa yang ingin saya tempatkan persentil tersebut.

19
00:01:17,000 --> 00:01:21,000
Anggap saja orang yang membuka aplikasi tersebut, 

20
00:01:21,000 --> 00:01:25,000
dan merupakan bagian dari 10% orang teratas.

21
00:01:25,000 --> 00:01:29,000
Selanjutnya, saya akan mengeklik "Terapkan."

22
00:01:29,000 --> 00:01:33,000
Sekarang Anda dapat melihat bagan

23
00:01:33,000 --> 00:01:37,000
diperbarui untuk menampilkan data untuk segmen ini. 

24
00:01:37,000 --> 00:01:41,000
Segmen ini belum disimpan, sehingga saya dapat 

25
00:01:41,000 --> 00:01:45,000
mengeklik "Lainnya" dan memilih "Simpan Sebagai." Sekarang saya dapat memberi 

26
00:01:45,000 --> 00:01:49,000
nama untuk segmen saya.

27
00:01:49,000 --> 00:01:53,000
Klik "Simpan Sebagai."

28
00:01:53,000 --> 00:01:57,000
Jika pada saat tertentu, saya ingin 

29
00:01:57,000 --> 00:02:01,000
kembali ke segmen tersebut, atau menyembunyikannya, saya dapat mengeklik "Sembunyikan"

30
00:02:01,000 --> 00:02:05,000
atau "Sunting." Dan saya dapat mengeklik menu pilihan segmen untuk memilih segmen lainnya.

31
00:02:05,000 --> 00:02:09,000
Dan begitulah caranya membuat Segmen di Analytics untuk Aplikasi.