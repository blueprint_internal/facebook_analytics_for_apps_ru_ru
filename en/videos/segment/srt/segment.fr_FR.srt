﻿1
00:00:05,000 --> 00:00:09,000
Il est facile de définir un segment dans

2
00:00:09,000 --> 00:00:13,000
Facebook Analytics pour apps 
dans un certain nombre de rapports différents.

3
00:00:13,000 --> 00:00:17,000
Je vais vous guider dans la définition d’un segment 
dans le rapport Revenus.

4
00:00:17,000 --> 00:00:21,000
Je clique sur Revenus.

5
00:00:21,000 --> 00:00:25,000
Je vois que le rapport n’est pas filtré du tout.

6
00:00:25,000 --> 00:00:29,000
Il affiche 100 % des utilisateurs.

7
00:00:29,000 --> 00:00:33,000
Mais je peux appliquer un segment
en cliquant sur Modifier.

8
00:00:33,000 --> 00:00:37,000
Je peux alors cliquer sur
Sélectionner une condition et choisir parmi

9
00:00:37,000 --> 00:00:41,000
différents types de conditions, notamment
Évènement, Données démographiques,

10
00:00:41,000 --> 00:00:45,000
Informations de l’appareil
ou Source d’installation.

11
00:00:45,000 --> 00:00:49,000
Je peux également ajouter une condition de pourcentage
pour un filtrage des personnes

12
00:00:49,000 --> 00:00:53,000
qui accomplissent le plus une action particulière.
Dans cet exemple,

13
00:00:53,000 --> 00:00:57,000
je vais créer une condition
en fonction des données démographiques.

14
00:00:57,000 --> 00:01:01,000
Je sélectionne un paramètre de Pays.

15
00:01:01,000 --> 00:01:05,000
Je clique dans le champ

16
00:01:05,000 --> 00:01:09,000
et sélectionne États-Unis d’Amérique.

17
00:01:09,000 --> 00:01:13,000
Ensuite, je clique sur Ajouter un percentile.

18
00:01:13,000 --> 00:01:17,000
Je sélectionne l’évènement
sur lequel je vais baser le pourcentage.

19
00:01:17,000 --> 00:01:21,000
Disons, les personnes qui lancent l’application

20
00:01:21,000 --> 00:01:25,000
et font partie des 10 % supérieurs.

21
00:01:25,000 --> 00:01:29,000
Je clique sur Appliquer.

22
00:01:29,000 --> 00:01:33,000
Vous voyez que le graphique est mis à jour

23
00:01:33,000 --> 00:01:37,000
pour afficher les données
pour ce segment.

24
00:01:37,000 --> 00:01:41,000
Ce segment n’est pas encore enregistré.

25
00:01:41,000 --> 00:01:45,000
Je peux cliquer sur Plus et Enregistrer sous.
Je donne un nom

26
00:01:45,000 --> 00:01:49,000
à mon segment.

27
00:01:49,000 --> 00:01:53,000
Je clique sur Enregistrer sous.

28
00:01:53,000 --> 00:01:57,000
À tout moment,

29
00:01:57,000 --> 00:02:01,000
je peux revenir au segment ou le masquer
en cliquant sur Modifier ou Masquer.

30
00:02:01,000 --> 00:02:05,000
Je peux aussi cliquer sur le menu déroulant
des segments pour en sélectionner d’autres.

31
00:02:05,000 --> 00:02:09,000
Voilà comment créer des segments
dans Analytics pour apps.