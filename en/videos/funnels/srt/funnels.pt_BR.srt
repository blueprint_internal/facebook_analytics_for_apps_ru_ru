﻿1
00:00:05,000 --> 00:00:09,000
Você pode criar um funil no Facebook Analytics para Aplicativos

2
00:00:09,000 --> 00:00:13,000
clicando em “Funis”, na seção “Atividade”.

3
00:00:13,000 --> 00:00:17,000
Em seguida, clique em “Criar funil”.

4
00:00:17,000 --> 00:00:21,000
Você pode adicionar etapas ao seu funil

5
00:00:21,000 --> 00:00:25,000
clicando em “Adicionar etapa do funil”. Neste caso, vamos criar um funil

6
00:00:25,000 --> 00:00:29,000
que represente um fluxo de comércio típico. Então, clico em “Adicionar etapa do funil”

7
00:00:29,000 --> 00:00:33,000
e, em seguida, procuro uma etapa do funil.

8
00:00:33,000 --> 00:00:37,000
Vou adicionar outra etapa do funil:

9
00:00:37,000 --> 00:00:41,000
“Adicionar ao carrinho”. Em seguida, adiciono uma etapa final:

10
00:00:41,000 --> 00:00:45,000
“Compras”. Observe que é possível

11
00:00:45,000 --> 00:00:49,000
refinar algumas dessas etapas, clicando em “Selecione o parâmetro”,

12
00:00:49,000 --> 00:00:53,000
e escolher algo como:

13
00:00:53,000 --> 00:00:57,000
“Evento de compra superior a US$ 50”. Depois de selecionar o parâmetro,

14
00:00:57,000 --> 00:01:01,000
clico em “Refinar” para concluir

15
00:01:01,000 --> 00:01:05,000
e, em seguida, clico em “Aplicar” para criar o funil.

16
00:01:09,000 --> 00:01:13,000
Depois de criar o funil,

17
00:01:13,000 --> 00:01:17,000
você pode consultar a diferença geral entre as etapas.

18
00:01:17,000 --> 00:01:21,000
Pode rolar para baixo para ver os valores numéricos para

19
00:01:21,000 --> 00:01:25,000
taxas de conversão, assim como o número de pessoas que chegaram a cada etapa do funil.

20
00:01:25,000 --> 00:01:29,000
Você também pode exibir o funil de acordo com diferentes atributos.

21
00:01:29,000 --> 00:01:33,000
Assim, detalhe-o por atributos, como idade ou gênero, e veja diferentes taxas de conversão.

22
00:01:33,000 --> 00:01:37,000
Você pode aplicar um segmento ao seu funil

23
00:01:37,000 --> 00:01:41,000
selecionando-o na lista suspensa de segmentos. Salve o funil

24
00:01:41,000 --> 00:01:45,000
clicando em “Salvar”.

25
00:01:45,000 --> 00:01:49,000
Nomeie-o e clique em “Salvar como”.

26
00:01:49,000 --> 00:01:53,000
E é assim que se cria um funil

27
00:01:53,000 --> 00:01:56,900
no Facebook Analytics para Aplicativos.

