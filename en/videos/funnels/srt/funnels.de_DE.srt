﻿1
00:00:05,000 --> 00:00:09,000
Du kann einen Funnel in Facebook Analytics für Apps erstellen,

2
00:00:09,000 --> 00:00:13,000
indem du im Abschnitt „Aktivität“ auf „Funnel“ klickst.

3
00:00:13,000 --> 00:00:17,000
Klicke als nächstes auf „Funnel erstellen“.

4
00:00:17,000 --> 00:00:21,000
Du kannst deinem Funnel Schritte hinzufügen,

5
00:00:21,000 --> 00:00:25,000
indem du auf „Funnelschritt hinzufügen“ klickst. In diesem Fall erstellen wir einen

6
00:00:25,000 --> 00:00:29,000
Funnel, der einen typischen Ablauf im Handel darstellt. Ich klicke also auf 

7
00:00:29,000 --> 00:00:33,000
„Funnelschritt hinzufügen“ und suche dann einen Funnelschritt.

8
00:00:33,000 --> 00:00:37,000
Ich füge einen weiteren Funnelschritt hinzu,

9
00:00:37,000 --> 00:00:41,000
und zwar „In den Einkaufswagen“. Dann füge ich einen letzten

10
00:00:41,000 --> 00:00:45,000
Funnelschritt hinzu, und zwar „Käufe“. Beachte Folgendes: 

11
00:00:45,000 --> 00:00:49,000
Du kannst einige der Schritte verfeinern, indem du auf „Parameter auswählen“ klickst,

12
00:00:49,000 --> 00:00:53,000
und z. B. „Kaufereignis

13
00:00:53,000 --> 00:00:57,000
größer als 50 $“ auswählen. Wenn ich damit

14
00:00:57,000 --> 00:01:01,000
fertig bin, klicke ich auf „Verfeinern“, um zu schließen.

15
00:01:01,000 --> 00:01:05,000
Dann klicke ich auf „Anwenden“, um meinen Funnel zu erstellen.

17
00:01:09,000 --> 00:01:13,000
Sobald der Funnel erstellt ist,

18
00:01:13,000 --> 00:01:17,000
kannst du die wichtigsten Unterschiede zwischen den Schritten betrachten.

19
00:01:17,000 --> 00:01:21,000
Scrolle nach unten, um die numerischen Werte für die Conversion Rates 

20
00:01:21,000 --> 00:01:25,000
sowie die Anzahl der Personen zu sehen, die die einzelnen Schritte im Funnel durchlaufen haben.

21
00:01:25,000 --> 00:01:29,000
Du kannst den Funnel auch nach verschiedenen Attributen anzeigen.

22
00:01:29,000 --> 00:01:33,000
Schlüssele ihn beispielsweise nach Alter oder Geschlecht auf und sieh dir die verschiedenen Conversion Rates dafür an.

23
00:01:33,000 --> 00:01:37,000
Du kannst bei deinem Funnel ein Segment anwenden,

24
00:01:37,000 --> 00:01:41,000
indem du das Segmentmenü auswählst. Außerdem kannst du 

25
00:01:41,000 --> 00:01:45,000
den Funnel speichern. Klicke hierzu auf „Speichern“. 

26
00:01:45,000 --> 00:01:49,000
Gib einen Namen ein und klicke auf „Speichern unter“.

27
00:01:49,000 --> 00:01:53,000
So erstellst du also einen Funnel

28
00:01:53,000 --> 00:01:56,900
in Facebook Analytics für Apps.