
function Icon_animation_stack(resources)
{
	Icon_animation_stack.resources = resources;
}
Icon_animation_stack.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(454, 752, Phaser.CANVAS, 'icon_animation_stack', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this },null,null,false);
	},

	preload: function()
	{
		
		this.game.scale.maxWidth = 454;
    	this.game.scale.maxHeight = 752;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		//this.game.load.image('placeholder',Icon_animation_stack.resources.placeholder);
		this.game.load.image('icon_1',Icon_animation_stack.resources.icon_1);
        this.game.load.image('icon_2',Icon_animation_stack.resources.icon_2);
        this.game.load.image('icon_3',Icon_animation_stack.resources.icon_3);
        this.game.load.image('icon_4',Icon_animation_stack.resources.icon_4);
        this.game.load.image('arrow',Icon_animation_stack.resources.arrow);
		//debugger
		
		this.game.created = false;
    	
    	
    	
  
    	this.game.stage.backgroundColor = '#ffffff';
    	//0f2747
    	
	},

	create: function(evt)
	{
		
		if(this.game.created === false)
		{
            //this.parent.placeholder = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY,'placeholder');
			//this.parent.placeholder.anchor.set(0.5,0.5);
			this.parent.laptop = this.game.make.sprite(0,0,'icon_1');
			this.parent.laptop.anchor.set(0.5,0.5);
            this.parent.game.stage.backgroundColor = '#ffffff';
			this.game.created  = true;
	    	this.parent.buildAnimation();
	    }
	},
    
	buildAnimation: function()
	{
		var style = Icon_animation_stack.resources.textStyle_1;
        this.icon_1= this.game.add.sprite(this.game.world.centerX+10,this.game.world.centerY-280,'icon_1');
        this.icon_1.anchor.set(0.5,0.5);
        this.icon_1.alpha = 0;
        
        
        this.arrow_1= this.game.add.sprite(this.game.world.centerX+10,this.game.world.centerY-195,'arrow');
        this.arrow_1.anchor.set(0.5,0.5);
        this.arrow_1.alpha = 0;
        
        //
        
        var text_1 = this.game.add.text(this.game.world.centerX+15,this.game.world.centerY-47, Icon_animation_stack.resources.text_1, style);
        text_1.anchor.set(0.5,0.5);
        
        this.icon_2= this.game.add.sprite(this.game.world.centerX+15,this.game.world.centerY-110,'icon_2');
        this.icon_2.anchor.set(0.5,0.5);
        this.icon_2.alpha = .5;
        
        var text_2 = this.game.add.text(this.game.world.centerX+15,this.game.world.centerY+145, Icon_animation_stack.resources.text_2, style);
        text_2.anchor.set(0.5,0.5);
        
        this.arrow_2= this.game.add.sprite(this.game.world.centerX+10,this.game.world.centerY-7,'arrow');
        this.arrow_2.anchor.set(0.5,0.5);
        this.arrow_2.alpha = .5;
        
        
        this.icon_3= this.game.add.sprite(this.game.world.centerX,this.game.world.centerY+80,'icon_3');
        this.icon_3.anchor.set(0.5,0.5);
        this.icon_3.alpha = .5;
        
        this.arrow_3= this.game.add.sprite(this.game.world.centerX+10,this.game.world.centerY+185,'arrow');
        this.arrow_3.anchor.set(0.5,0.5);
        this.arrow_3.alpha = .5;
        
        
        
        this.icon_4= this.game.add.sprite(this.game.world.centerX+7,this.game.world.centerY+290,'icon_4');
        this.icon_4.anchor.set(0.5,0.5);
        this.icon_4.alpha = .5;
        
        this.icon_1_An = this.game.add.tween(this.icon_1).to( { alpha:1,y:this.icon_1.y},500,Phaser.Easing.Quadratic.Out);
        this.icon_1.y-=10;
        this.icon_1.alpha = 0;
        
        
       this.arrow_1_anim = this.game.add.tween(this.arrow_1).to({alpha:1},300,Phaser.Easing.Quadratic.Out);
       this.arrow_1.alpha = 0;
        
       this.icon_2_An = this.game.add.tween(this.icon_2).to( { alpha:1,y:this.icon_2.y},500,Phaser.Easing.Quadratic.Out);
       this.icon_2.y-=10;
       this.icon_2.alpha = 0;
        
       this.text_1_anim = this.game.add.tween(text_1).to({alpha:1,x:text_1.x},500,Phaser.Easing.Quadratic.Out);
       text_1.alpha = 0;
       text_1.x+=10;
        
       this.arrow_2_anim = this.game.add.tween(this.arrow_2).to({alpha:1},300,Phaser.Easing.Quadratic.Out);
       this.arrow_2.alpha = 0;
        
       this.icon_3_An = this.game.add.tween(this.icon_3).to( { alpha:1,y:this.icon_3.y},500,Phaser.Easing.Quadratic.Out);
       this.icon_3.y-=10;
       this.icon_3.alpha = 0;
        
       this.text_2_anim = this.game.add.tween(text_2).to({alpha:1,x:text_2.x},500,Phaser.Easing.Quadratic.Out);
       text_2.alpha = 0;
       text_2.x-=10;
        
       this.arrow_3_anim = this.game.add.tween(this.arrow_3).to({alpha:1},300,Phaser.Easing.Quadratic.Out);
       this.arrow_3.alpha = 0;
        
       this.icon_4_An = this.game.add.tween(this.icon_4).to( { alpha:1,y:this.icon_4.y},500,Phaser.Easing.Quadratic.Out);
       this.icon_4.y-=10;
       this.icon_4.alpha = 0;
       
        
        
        
        
        
        
        
        
       this.icon_1_An.chain(this.arrow_1_anim,this.icon_2_An,this.text_1_anim,this.arrow_2_anim,this.icon_3_An,this.text_2_anim,this.arrow_3_anim,this.icon_4_An);
        
       this.icon_1_An.start();
		
    },
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}
//course/en/animations/nielsen_audience/

//grey 908f8f


